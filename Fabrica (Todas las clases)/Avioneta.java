package Fabrica;
import javax.swing.JOptionPane;

public class Avioneta extends Vehiculo implements PuedeVolar {
	
	private String aeropuerto = "Barcelona-El Prat";
	private static double maxKg = 150;
	
	public Avioneta(){
		super();
		setMatricula(matAleatoria());
		setMarca("AeroStar");
		setModelo("Project");
		setColor("Blanco");
		setKilometros(0.0);
		setNumPuertas(2);
		setNumPlazas(4);
		aeropuerto = "Barcelona-El Prat";
		maxKg = 150;
	}
	public Avioneta(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, String aeropuerto, double maxKg) {
		super(matricula, marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.aeropuerto = aeropuerto;
		this.maxKg = maxKg;
	}

	public String getAeropuerto() {
		return aeropuerto;
	}

	public void setAeropuerto(String aeropuerto) {
		this.aeropuerto = aeropuerto;
	}

	public static double getMaxKg() {
		return maxKg;
	}

	public void setMaxKg(double maxKg) {
		this.maxKg = maxKg;
	}

	@Override
	public void volar() {
		JOptionPane.showMessageDialog(null, "Esto es un avi�n y su recorrido principal lo hace en el aire, movi�ndose atrav�s de aeropuertos.");
	}

	@Override
	public void arrancar() {
		System.out.println("El veh�culo est� siendo arrancado.");
	}

	@Override
	public void acelerar() {
		System.out.println("El veh�culo est� acelerando.");
	}

	@Override
	public void frenar() {
		System.out.println("El veh�culo est� frenando.");
	}

	@Override
	public String toString() {
		return "Matricula: "+getMatricula()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nColor: "+getColor()+"\nKil�metros: "+getKilometros()+"\nN�mero de puertas: "+getNumPuertas()+"\nN�mero de plazas: "+getNumPlazas()+
				"\nAeropuerto de alojamiento: "+getAeropuerto()+"\nPeso m�ximo: "+getMaxKg()+" kg";
	}
	
	public void despegar(){
		System.out.println("La avioneta est� despegando y lista para volar.");
	}
	
	public void aterrizar(){
		System.out.println("La avioneta est� lista para aterrizar. Buscando lugar m�s cercano para realizar la maniobra...");
	}
	
}
