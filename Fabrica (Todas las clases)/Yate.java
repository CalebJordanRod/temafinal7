package Fabrica;
import javax.swing.JOptionPane;

public class Yate extends Vehiculo implements PuedeNavegar {
	
	private static boolean tieneCocina = false;
	private int numMotores = 2;
	private double metrosEslora = 16.5;
	
	public Yate (){
		super();
		setMatricula(matAleatoria());
		setMarca("Sea Ray");
		setModelo("Sun Dancer");
		setColor("Gris");
		setKilometros(0.0);
		setNumPuertas(1);
		setNumPlazas(10);
		tieneCocina = false;
		numMotores = 2;
		metrosEslora = 16.5;
	}
	public Yate(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, int numMotores, double metrosEslora) {
		super(matricula, marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.numMotores = numMotores;
		this.metrosEslora = metrosEslora;
	}

	public static String getTieneCocina() {
		if(tieneCocina == true){
			return "Incorporado";
		}else {
			return "No incorporado";
		}
	}
	
	public void setTieneCocina(boolean tieneCocina) {
		this.tieneCocina = tieneCocina;
	}
	
	public int getNumMotores() {
		return numMotores;
	}
	
	public void setNumMotores(int numMotores) {
		this.numMotores = numMotores;
	}
	
	public double getMetrosEslora() {
		return metrosEslora;
	}
	
	public void setMetrosEslora(double metrosEslora) {
		this.metrosEslora = metrosEslora;
	}
	
	@Override
	public void navegar() {
		JOptionPane.showMessageDialog(null, "Esto es un barco y los barcos circulan por el agua, tanto mares, como oc�anos y r�os.");
	}

	@Override
	public void arrancar() {
		System.out.println("El veh�culo est� siendo arrancado.");
	}

	@Override
	public void acelerar() {
		System.out.println("El veh�culo est� acelerando.");
	}

	@Override
	public void frenar() {
		System.out.println("El veh�culo est� frenando.");
	}

	@Override
	public String toString() {
		return "Matricula: "+getMatricula()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nColor: "+getColor()+"\nKil�metros: "+getKilometros()+"\nN�mero de puertas: "+getNumPuertas()+"\nN�mero de plazas: "+getNumPlazas()+
				"\nCocina: "+getTieneCocina()+"\nN�mero de motores: "+getNumMotores()+"\nMetros de eslora: "+getMetrosEslora();
	}
	
	public void zarpar(){
		System.out.println("El barco est� zarpando.");
	}
	public void atracar(){
		System.out.println("El barco est� atracando.");
	}

}
