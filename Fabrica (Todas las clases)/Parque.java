package Fabrica;

import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Parque {

	public static void main(String[] args) {
		
		boolean end = false;
		String s;
		ArrayList <Vehiculo> fabrica = new ArrayList <Vehiculo> ();
		
		do {
			s = JOptionPane.showInputDialog(null, "Fabrica de veh�culos Jordan Rodr�guez. Selecciona una de las siguientes opciones:\n\n" +
				"1. Fabricar coche.\n" +
				"2. Fabricar autob�s\n" +
				"3. Fabricar motocicleta\n" +
				"4. Fabricar avioneta\n" +
				"5. Fabricar Yate\n" +
				"6. Mostrar caracter�sticas de todos los veh�culos del parque\n" +
				"7. Mostrar caracter�sticas de un veh�culo espec�fico\n" +
				"8. Ver matr�culas del parque de veh�culos disponibles\n" +
				"9. Salir del programa.\n\n","�Bienvenido a mi f�brica de veh�culos!", JOptionPane.PLAIN_MESSAGE);
			
			switch (s) {
				
			case "1":{
				if (Vehiculo.getNumCoches() < Vehiculo.getMaxCoches()){
					String conf;
					int conf1;
					conf = JOptionPane.showInputDialog("�Fabricar coche con datos? ('1')\nDatos por defecto ('0')");
					conf1 = Integer.parseInt(conf);
					if (conf1 == 1){
						int nPi, nPli;
						double kd;
						String m = JOptionPane.showInputDialog(null,"Inserte matr�cula del coche:");
						String ma = JOptionPane.showInputDialog(null,"Inserte marca:");
						String mo = JOptionPane.showInputDialog(null,"Inserte modelo:");
						String c = JOptionPane.showInputDialog(null,"Inserte color:");
						String k = JOptionPane.showInputDialog(null,"Inserte kil�metros:");
						kd = Double.parseDouble(k);
						String nP = JOptionPane.showInputDialog(null,"Inserte el n�mero de puertas:");
						nPi = Integer.parseInt(nP);
						String nPl = JOptionPane.showInputDialog(null,"Inserte el n�mero de plazas:");
						nPli = Integer.parseInt(nPl);
						Coche coche = new Coche(m, ma, mo, c, kd, nPi, nPli);
						fabrica.add(coche);
						JOptionPane.showMessageDialog(null, coche.toString());
					} else if (conf1 == 0){
						Coche coche1 = new Coche();
						JOptionPane.showMessageDialog(null, "Se ha fabricado un coche con datos por defecto");
						fabrica.add(coche1);
						JOptionPane.showMessageDialog(null, coche1.toString());
					}
				}else{
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� lleno");
				}
				break;
			}
			case "2":{
				if (Vehiculo.getNumCoches() < Vehiculo.getMaxCoches()){
					String conf;
					int conf1;
					conf = JOptionPane.showInputDialog("�Fabricar autob�s con datos? ('1')\nDatos por defecto ('0')");
					conf1 = Integer.parseInt(conf);
					if (conf1 == 1){
						int nPi, nPli;
						double kd;
						String m = JOptionPane.showInputDialog(null,"Inserte matr�cula del autob�s:");
						String ma = JOptionPane.showInputDialog(null,"Inserte marca:");
						String mo = JOptionPane.showInputDialog(null,"Inserte modelo:");
						String c = JOptionPane.showInputDialog(null,"Inserte color:");
						String k = JOptionPane.showInputDialog(null,"Inserte kil�metros:");
						kd = Double.parseDouble(k);
						String nP = JOptionPane.showInputDialog(null,"Inserte el n�mero de puertas:");
						nPi = Integer.parseInt(nP);
						String nPl = JOptionPane.showInputDialog(null,"Inserte el n�mero de plazas:");
						nPli = Integer.parseInt(nPl);
						Autobus autobus = new Autobus(m, ma, mo, c, kd, nPi, nPli);
						fabrica.add(autobus);
						JOptionPane.showMessageDialog(null, autobus.toString());
					}else if (conf1 == 0){
						Autobus autobus1 = new Autobus();
						fabrica.add(autobus1);
						JOptionPane.showMessageDialog(null, "Se ha fabricado un autob�s con datos por defecto");
						JOptionPane.showMessageDialog(null, autobus1.toString());
						
						
					}
				}else{
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� lleno");
				}
				break;
			}
			case "3":{
				if (Vehiculo.getNumCoches() < Vehiculo.getMaxCoches()){
					String conf;
					int conf1;
					conf = JOptionPane.showInputDialog("�Fabricar motocicleta con datos? ('1')\nDatos por defecto ('0')");
					conf1 = Integer.parseInt(conf);
					if (conf1 == 1){
						int nPli, pi;
						double kd;
						String m = JOptionPane.showInputDialog(null,"Inserte matr�cula de la motocicleta:");
						String ma = JOptionPane.showInputDialog(null,"Inserte marca:");
						String mo = JOptionPane.showInputDialog(null,"Inserte modelo:");
						String c = JOptionPane.showInputDialog(null,"Inserte color:");
						String k = JOptionPane.showInputDialog(null,"Inserte kil�metros:");
						kd = Double.parseDouble(k);
						String nPl = JOptionPane.showInputDialog(null,"Inserte el n�mero de plazas:");
						nPli = Integer.parseInt(nPl);
						String p = JOptionPane.showInputDialog("Inserte la potencia del motor deseada:");
						pi = Integer.parseInt(p);
						Motocicleta moto = new Motocicleta(m, ma, mo, c, kd, 0, nPli, pi);
						fabrica.add(moto);
						JOptionPane.showMessageDialog(null, moto.toString());
					}else if (conf1 == 0){
						Motocicleta moto1 = new Motocicleta();
						fabrica.add(moto1);
						JOptionPane.showMessageDialog(null, "Se ha fabricado una motocicleta con datos por defecto");
						JOptionPane.showMessageDialog(null, moto1.toString());
					}
				}else{
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� lleno");
				}
				break;
			}
			case "4":{
				if (Vehiculo.getNumCoches() < Vehiculo.getMaxCoches()){
					String conf;
					int conf1;
					conf = JOptionPane.showInputDialog("�Fabricar avioneta con datos? ('1')\nDatos por defecto ('0')");
					conf1 = Integer.parseInt(conf);
					if (conf1 == 1){
						int nPi, nPli;
						double kd, mk;
						String m = JOptionPane.showInputDialog(null,"Inserte matr�cula del avioneta:");
						String ma = JOptionPane.showInputDialog(null,"Inserte marca:");
						String mo = JOptionPane.showInputDialog(null,"Inserte modelo:");
						String c = JOptionPane.showInputDialog(null,"Inserte color:");
						String k = JOptionPane.showInputDialog(null,"Inserte kil�metros:");
						kd = Double.parseDouble(k);
						String nP = JOptionPane.showInputDialog(null,"Inserte el n�mero de puertas:");
						nPi = Integer.parseInt(nP);
						String nPl = JOptionPane.showInputDialog(null,"Inserte el n�mero de plazas:");
						nPli = Integer.parseInt(nPl);
						String a = JOptionPane.showInputDialog("Aeropuerto de alojamiento");
						String mx = JOptionPane.showInputDialog("Kilogramos m�ximos:");
						mk = Double.parseDouble(mx);
						Avioneta avion = new Avioneta(m, ma, mo, c, kd, nPi, nPli, a, mk);
						fabrica.add(avion);
						JOptionPane.showMessageDialog(null, avion.toString());
					}else if (conf1 == 0){
						Avioneta avion1 = new Avioneta ();
						fabrica.add(avion1);
						JOptionPane.showMessageDialog(null, "Se ha fabricado una avioneta con datos por defecto");
						JOptionPane.showMessageDialog(null, avion1.toString());
					}
				}else{
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� lleno");
				}
				break;
			}
			case "5":{
				if (Vehiculo.getNumCoches() < Vehiculo.getMaxCoches()){
					String conf;
					int conf1;
					conf = JOptionPane.showInputDialog("�Fabricar yate con datos? ('1')\nDatos por defecto ('0')");
					conf1 = Integer.parseInt(conf);
					if (conf1 == 1){
						int nPi, nPli, nmi;
						double kd, med; 
						String m = JOptionPane.showInputDialog(null,"Inserte matr�cula del yate:");
						String ma = JOptionPane.showInputDialog(null,"Inserte marca:");
						String mo = JOptionPane.showInputDialog(null,"Inserte modelo:");
						String c = JOptionPane.showInputDialog(null,"Inserte color:");
						String k = JOptionPane.showInputDialog(null,"Inserte kil�metros:");
						kd = Double.parseDouble(k);
						String nP = JOptionPane.showInputDialog(null,"Inserte el n�mero de puertas:");
						nPi = Integer.parseInt(nP);
						String nPl = JOptionPane.showInputDialog(null,"Inserte el n�mero de plazas:");
						nPli = Integer.parseInt(nPl);
						String nm = JOptionPane.showInputDialog(null, "Inserte n�mero de motores");
						nmi = Integer.parseInt(nm);
						String me = JOptionPane.showInputDialog("Inserte los metros de eslora del yate:");
						med = Double.parseDouble(me);
						Yate yate = new Yate(m, ma, mo, c, kd, nPi, nPli, nmi, med);
						fabrica.add(yate);
						JOptionPane.showMessageDialog(null, yate.toString());
					}else if (conf1 == 0){
						Yate yate1 = new Yate();
						fabrica.add(yate1);
						JOptionPane.showMessageDialog(null, "Se ha fabricado un yate con datos por defecto");
						JOptionPane.showMessageDialog(null, yate1.toString());
					}
				}else{
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� lleno");
				}
				break;
			}
			case "6":{
				if (fabrica.isEmpty()){
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� vac�o.");
				}else {
					mostrarArray(fabrica);
				}
				break;
			}
			case "7":{
				if (fabrica.isEmpty()){
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� vac�o.");
				}else {
					String t = JOptionPane.showInputDialog(null,"Introduce la matr�cula del veh�culo: ");
					int b = buscaMatricula(fabrica, t);
					if (b == -1){
						JOptionPane.showMessageDialog(null, "La matricula introducida no coincide con ningun veh�culo");
					}else {
						JOptionPane.showMessageDialog(null, fabrica.get(b));
					}
				}
				break;
			}
			case "8":{
				if (fabrica.isEmpty()){
					JOptionPane.showMessageDialog(null, "El parque de veh�culos est� vac�o.");
				}else {
					for(int i = 0; i < fabrica.size(); i++){
						JOptionPane.showMessageDialog(null, "Matricula disponible: " +fabrica.get(i).getMatricula());
					}
				}
				break;
			}
			case "9": {
				JOptionPane.showMessageDialog(null, "�Hasta pronto!");
				end = true;
				break;
			}
			}
			
		}while(end == false);
		
		
	}
	
    public static int buscaMatricula(ArrayList <Vehiculo> fabrica, String m)
    {
        for(int x = 0; x < fabrica.size(); x++){
        	if (fabrica.get(x) == null){
        		return -1;
        	}
            if(m.equals(fabrica.get(x).getMatricula())){
                return x;
            }
        }
        return -1;
    }
    
    public static int tipoVehiculo(Vehiculo v){
    		if (v instanceof Coche){
    			return 1;
    		}
    		if(v instanceof Autobus){
    			return 2;
    		}
    		if(v instanceof Motocicleta){
    			return 3;
    		}
    		if(v instanceof Avioneta){
    			return 4;
    		}
    		if(v instanceof Yate){
    			return 5;
    	}
    	return -1;
    }
    public static void mostrarArray(ArrayList <Vehiculo> fabrica){
    	
    	for (int i = 0; i < fabrica.size(); i++){
    		int b = tipoVehiculo(fabrica.get(i));
    		if (b == 1){
	    		JOptionPane.showMessageDialog(null, fabrica.get(i).toString());
	    		Coche co = (Coche) fabrica.get(i);
	    		co.circular();
    		}
    		if (b == 2){
        		JOptionPane.showMessageDialog(null, fabrica.get(i).toString());
        		Autobus au = (Autobus) fabrica.get(i);
        		au.circular();
        	}
    		if (b == 3){
        		JOptionPane.showMessageDialog(null, fabrica.get(i).toString());
        		Motocicleta mo = (Motocicleta) fabrica.get(i);
        		mo.circular();
        	}
    		if (b == 4){
        		JOptionPane.showMessageDialog(null, fabrica.get(i).toString());
        		Avioneta av = (Avioneta) fabrica.get(i);
        		av.volar();
        	}
    		if (b == 5){
        		JOptionPane.showMessageDialog(null, fabrica.get(i).toString());
        		Yate ya = (Yate) fabrica.get(i);
        		ya.navegar();
        	}
    		if (b == -1){
    			JOptionPane.showMessageDialog(null, "No se ha podido hayar ning�n veh�culo.");
    		}
    	}
    }
}
