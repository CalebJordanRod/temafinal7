package Fabrica;
import javax.swing.JOptionPane;

public class Autobus extends Vehiculo implements PuedeCircular {
	
	private static String tipoRecorrido = "Rural";
	private boolean esEscolar = false;
	
	public Autobus(){
		super();
		setMatricula(matAleatoria());
		setMarca("Volvo");
		setModelo("9700 Pax");
		setColor("Azul");
		setKilometros(0.0);
		setNumPuertas(2);
		setNumPlazas(30);
		tipoRecorrido = "Rural";
		esEscolar = false;
	}
	
	public Autobus(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas) {
		super(matricula, marca, modelo, color, kilometros, numPuertas, numPlazas);
		tipoRecorrido = "Rural";
		esEscolar = false;
	}

	public void setTipoRecorrido(String tipoRecorrido){
		this.tipoRecorrido = tipoRecorrido;
	}
	
	public static String getTipoRecorrido(){
		return tipoRecorrido;
	}
	
	public void setEsEscolar(boolean esEscolar){
		this.esEscolar = esEscolar;
	}
	
	public String getEsEscolar(){
		if (esEscolar == true){
			return "No";
		}else {
			return "S�";
		}
	}
	
	@Override
	public String toString(){
		return "Matricula: "+getMatricula()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nColor: "+getColor()+"\nKil�metros: "+getKilometros()+"\nN�mero de puertas: "+getNumPuertas()+"\nN�mero de plazas: "+getNumPlazas()+
				"\nTipo de recorrido: "+getTipoRecorrido()+"\nApto para transporte escolar: "+getEsEscolar();
	}
	
	@Override
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es un autob�s y los autobuses puede circular por carreteras, autov�as y autopistas.");
	}

	@Override
	public void arrancar() {
		System.out.println("El veh�culo est� siendo arrancado.");
	}

	@Override
	public void acelerar() {
		System.out.println("El veh�culo est� acelerando.");
	}

	@Override
	public void frenar() {
		System.out.println("El veh�culo est� frenando.");
	}
	
	public void abrirPuertas(){
		System.out.println("Se abren las puertas del autob�s para que suban pasajeros.");
	}
	
	public void aparcar(){
		System.out.println("El autob�s est� siendo aparcado");
	}

}
