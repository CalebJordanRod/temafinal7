package Fabrica;
import javax.swing.JOptionPane;

public class Coche extends Vehiculo implements PuedeCircular {
		
		private static int numAirBags = 2;
		private boolean techoSolar = false;
		private boolean tieneRadio = true;
		
	public Coche (){
		super();
		setMatricula(matAleatoria());
		setMarca("Seat");
		setModelo("Altea");
		setColor("Blanco");
		setKilometros(0.0);
		setNumPuertas(3);
		setNumPlazas(5);
		techoSolar = false;
		numAirBags = 2;
		tieneRadio = true;
	}
	
	public Coche(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas) {
		super(matricula, marca, modelo, color, kilometros, numPuertas, numPlazas);
		techoSolar = false;
		numAirBags = 2;
		tieneRadio = true;
	}

	public static int getNumAirBags() {
		return numAirBags;
	}
	
	public void setNumAirBags(int numAirBags) {
		this.numAirBags = numAirBags;
	}
	
	public String getTechoSolar (){
		if (techoSolar == true){
			return "Incorporado";
		}
		else {
			return "No incorporado";
		}
	}
	
	public void setTechoSolar(boolean techoSolar) {
		this.techoSolar = techoSolar;
	}
	
	public String getTieneRadio() {
		if(tieneRadio == true){
			return "Incorporado";
		}else {
			return "No incorporado";
		}
	}
	
	public void setTieneRadio(boolean tieneRadio) {
		this.tieneRadio = tieneRadio;
	}
	
	@Override
	public String toString(){
		return "Matricula: "+getMatricula()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nColor: "+getColor()+"\nKil�metros: "+getKilometros()+"\nN�mero de puertas: "+getNumPuertas()+"\nN�mero de plazas: "+getNumPlazas()+
				"\nTecho Solar: "+getTechoSolar()+"\nN�mero de airbags incluidos: "+getNumAirBags()+"\nRadio: "+getTieneRadio();
	}
	
	@Override
	public void circular(){
		JOptionPane.showMessageDialog(null, "Esto es un coche y los coches puede circular por carreteras, autov�as y autopistas.");
	}
	
	@Override
	public void arrancar(){
		System.out.println("El veh�culo est� siendo arrancado.");
	}
	
	@Override
	public void acelerar(){
		System.out.println("El veh�culo est� acelerando.");
	}
	
	@Override
	public void frenar(){
		System.out.println("El veh�culo est� frenando.");
	}
	
	public void tunear(String color){
		setColor(color);
		setKilometros(0.0);
		if (techoSolar == false){
			this.techoSolar = true;
		}
		//Mensaje JOptionPane
	}
	
	public void aparcar(){
		System.out.println("El coche est� siendo aparcado");
	}
}
