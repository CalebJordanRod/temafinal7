package Fabrica;

public abstract class Vehiculo {
	
	private String matricula;
	private String marca;
	private String modelo;
	private String color = "Blanco";
	private double kilometros = 0.0;
	private int numPuertas;
	private int numPlazas;
	private static int numCoches = 0;
	private static final int MAX_COCHES = 5;
	
	public Vehiculo(){
		matricula = matAleatoria();
		marca = "";
		modelo = "";
		color = "Blanco";
		kilometros = 0.0;
		numPuertas = 3;
		numPlazas = 5;
		numCoches++;
	}
	public Vehiculo (String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas){
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;
		this.kilometros = kilometros;
		this.numPuertas = numPuertas;
		this.numPlazas = numPlazas;
		numCoches++;
	}
	
	public String getMatricula() {
		return matricula;
	}
	
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public String getModelo() {
		return modelo;
	}
	
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		this.color = color;
	}
	
	public double getKilometros() {
		return kilometros;
	}
	
	public void setKilometros(double kilometros) {
		this.kilometros = kilometros;
	}
	
	public int getNumPuertas() {
		return numPuertas;
	}
	
	public void setNumPuertas(int numPuertas) {
		this.numPuertas = numPuertas;
	}
	
	public int getNumPlazas() {
		return numPlazas;
	}
	
	public void setNumPlazas(int numPlazas) {
		this.numPlazas = numPlazas;
	}
	
	public static int getNumCoches(){
		return numCoches;
	}
	
	public static int getMaxCoches(){
		return MAX_COCHES;
	}
	
	public abstract String toString();
	
	public abstract void arrancar();
	
	public abstract void acelerar();
	
	public abstract void frenar();
	
	public static String matAleatoria(){
		int r = (int) (1 + Math.random()*9);
		int a = (int) (1 + Math.random()*9);
		int n = (int) (1 + Math.random()*9);
		int d = (int) (1 + Math.random()*9);
		int o = (int) (1 + Math.random()*9);
		String m = (r + "" + a + "" + n + "" + d + "" + o);
		return m;
		
	}
}
