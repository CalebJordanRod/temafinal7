package Fabrica;
import javax.swing.JOptionPane;

public class Motocicleta extends Vehiculo implements PuedeCircular {
	
	private int potenciaMotor = 100;
	private static boolean tieneMaletero = true;
	
	public Motocicleta(){
		super();
		setMatricula(matAleatoria());
		setMarca("Yamaha");
		setModelo("TZR");
		setColor("Negro");
		setKilometros(0.0);
		setNumPuertas(0);
		setNumPlazas(2);
		potenciaMotor = 100;
		tieneMaletero = true;
	}
	
	public Motocicleta(String matricula, String marca, String modelo, String color, double kilometros, int numPuertas, int numPlazas, int potenciaMotor) {
		super(matricula, marca, modelo, color, kilometros, numPuertas, numPlazas);
		this.potenciaMotor = potenciaMotor;
		tieneMaletero = true;
	}
	
	public int getPotenciaMotor() {
		return potenciaMotor;
	}

	public void setPotenciaMotor(int potenciaMotor) {
		this.potenciaMotor = potenciaMotor;
	}

	public static String getTieneMaletero() {
		if(tieneMaletero == true){
			return "Incorporado";
		}else {
			return "No incorporado";
		}
	}

	public void setTieneMaletero(boolean tieneMaletero) {
		this.tieneMaletero = tieneMaletero;
	}
	
	@Override
	public String toString(){
		return "Matricula: "+getMatricula()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nColor: "+getColor()+"\nKil�metros: "+getKilometros()+"\nN�mero de puertas: "+getNumPuertas()+"\nN�mero de plazas: "+getNumPlazas()+
				"\nPotencia del motor: "+getPotenciaMotor()+" caballos de potencia"+"\nMaletero: "+getTieneMaletero();
	}

	@Override
	public void circular() {
		JOptionPane.showMessageDialog(null, "Esto es una motocicleta y las motocicletas puede circular por carreteras, autov�as y autopistas, depende tambi�n de su cilindrada.");
	}

	@Override
	public void arrancar() {
		System.out.println("El veh�culo est� siendo arrancado.");
	}

	@Override
	public void acelerar() {
		System.out.println("El veh�culo est� acelerando.");
	}

	@Override
	public void frenar() {
		System.out.println("El veh�culo est� frenando.");
	}
	
	public void brincar(){
		System.out.println("La moto est� brincando.");
	}
	
	public void aparcar(){
		System.out.println("La moto est� siendo aparcada");
	}

}
